# coding: utf-8
from django import template

from sbits_pages.models import SCMS_News

register = template.Library()


@register.inclusion_tag(
    'scms/last_pages.html', takes_context=True)
def last_pages_for_category(context, category_id, limit):
    current = context['page']
    news = SCMS_News.objects \
        .filter(category__id=category_id) \
        .exclude(id=current.id) \
        .order_by('-vip', 'create_date')[:limit]
    context.update({'news': news})
    return context
